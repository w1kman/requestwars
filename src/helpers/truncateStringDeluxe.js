export default function truncateStringDeluxe(str, limit) {
    const truncLimit = limit - 3; // since three dots...
    if (str.length <= limit) return str;
    return str.substr(0, str.lastIndexOf(' ', truncLimit)) + '...';
}
