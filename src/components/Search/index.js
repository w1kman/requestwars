import { connect } from 'react-redux';
import Search from './Search';
import { searchCharacters } from 'store/search/actions';

function mapStateToProps(state) {
  const {
    query,
    isSearching,
  } = state.search;

  return {
    query,
    isSearching,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSearch(query) {
      dispatch(searchCharacters(query));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
