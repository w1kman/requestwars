import React from 'react';
import PropTypes from 'prop-types';
import TextInput from 'common/TextInput';
import Button from 'common/Button';
import SearchContainer, { InputContainer} from './styled';
/**
 * Search Component
 */
export default class Search extends React.PureComponent {
  /**
   * Search default props
   * @type {{query: string, isSearching: boolean, items: Array}}
   */
  static defaultProps = {
    query: '',
    isSearching: false,
  };

  constructor(props) {
    super(props);

    // Initialize state
    const { query } = props;
    this.state = { query };

    // Bind event callbacks
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  /**
   * Make sure app state and component state is synced
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    if ((this.props.query !== prevProps.query) && (this.props.query !== this.state.query)) {
      this.setState({ query: this.props.query });
    }
  }

  /**
   * Handles change events for the search input element/component
   * @param {SyntheticKeyboardEvent} event
   */
  onSearchChange(event) {
    const { target } = event;
    if (target) {
      this.setState({ query: String(target.value) });
    }
  }

  /**
   * Dispatches a searchCharacter action when a user clicks the "submit" button.
   */
  onSearchSubmit() {
    const { onSearch } = this.props;
    const { query } = this.state;
    onSearch(query);
  }

  /**
   *
   * @param {KeyboardEvent} event
   */
  onKeyDown(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.onSearchSubmit();
    }
  }

  render() {
    const { isSearching } = this.props;
    return (
      <SearchContainer>
        <InputContainer>
          <TextInput
            title="huuguughghg raaaaaahhgh aaaaahnr"
            value={this.state.query}
            onChange={this.onSearchChange}
            onKeyDown={this.onKeyDown}
            disabled={isSearching}
            placeholder="huuguughghg raaaaaahhgh aaaaahnre"
          />
          <Button
            title="uughguughhhghghghhhgh raaaaaahhgh"
            onClick={this.onSearchSubmit}
          >
            Search!
          </Button>
        </InputContainer>
      </SearchContainer>
    );
  }
}

/**
 * Search component prop types
 * Note: Default props are written as a static property inside the main class
 */
Search.propTypes = {
  query: PropTypes.string,
  isSearching: PropTypes.bool,
  onSearch: PropTypes.func.isRequired,
};

