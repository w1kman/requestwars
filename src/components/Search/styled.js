import styled from 'styled-components';
import Input from 'common/Input/styled';
import Button from 'common/Button';
import starwarsFont from 'common/css/starwarsFont';

export default styled('div')`
  width: 100%;
  padding: 1em;
  background: linear-gradient(150deg, rgba(255,63,87, 1) 50%, rgb(115,145,171));
  border-radius: 6px;
  box-shadow: 1px 1px 10px rgba(0,0,0,0.08) inset;
  margin: 0 1em;
`;

export const InputContainer = styled('div')`
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  
  & ${Input} {
    flex-grow: 1;
  }
  
  & ${Button} {
    ${starwarsFont};
    font-size: 20px;
    line-height: 24px;
    padding: 0 0 5px 0; /* Looked a bit nasty */
    margin-left: 0.5em;
    width: 150px;
    height: 50px;
  }
`;
