import {connect} from 'react-redux';
import WarpSpeedCanvas from './WarpSpeedCanvas';
import { getIsSearching } from 'store/search/reselect';

function mapStateToProps(state) {
  return {
    isLoading: getIsSearching(state),
  };
}


export default connect(mapStateToProps)(WarpSpeedCanvas);
