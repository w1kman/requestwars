import React from 'react';
import WarpSpeed from 'lib/warpspeed';
import PropTypes from 'prop-types';

export default class WarpSpeedCanvas extends React.Component {
  static canvasId = 'warp-speed-canvas';
  static settings = {
    "speed": 0.7,
    "speedAdjFactor": 0.2,
    "density": 0.7,
    "shape": "square",
    "warpEffect": true,
    "warpEffectLength": 5,
    "depthFade": true,
    "starSize": 3,
    "backgroundColor": "rgb(241,241,241)",
    "starColor": "#7a7a7a"
  };
  static defaultProps = {
    isLoading: false,
  };

  constructor(props) {
    super(props);

    this.canvasRef = React.createRef();
    this.onResize = this.onResize.bind(this);
  }

  onResize() {
    const canvas = this.canvasRef.current;
    if (canvas) {
      const scale = window.devicePixelRatio || 1;
      canvas.width = window.innerWidth / scale;
      canvas.height = window.innerHeight / scale;
    }
  }

  componentDidMount() {
    this.warpspeed = new WarpSpeed(
      WarpSpeedCanvas.canvasId,
      WarpSpeedCanvas.settings,
    );
    window.warpspeed = this.warpspeed;
    this.onResize();
    window.addEventListener('resize', this.onResize);
  }

  componentDidUpdate({ prevIsLoading }) {
    const { isLoading } = this.props;
    if (isLoading !== prevIsLoading) {
      if (isLoading) {
        this.warpspeed.TARGET_SPEED = 20;
      } else {
        this.warpspeed.TARGET_SPEED = WarpSpeedCanvas.settings.speed;
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  render() {
    return (
      <canvas ref={this.canvasRef} id="warp-speed-canvas"/>
    );
  }
}

WarpSpeedCanvas.propTypes = {
  isLoading: PropTypes.bool
};
