import React from 'react';
import PropTypes from 'prop-types';
import {PlanetText, PopulationText} from './styled';

/**
 * Renders homeworld information
 * @param {boolean} isLoading
 * @param {string} name
 * @param {number} population
 * @returns {*}
 * @constructor
 */
export default function PlanetInfo({ isLoading, name, population }) {
  return (
    <span>
      Homeworld: <PlanetText>{name}</PlanetText> <PopulationText>(population {population})</PopulationText>
    </span>
  );
}

PlanetInfo.defaultProps = {
  isLoading: true,
  name: PropTypes.string,
  population: PropTypes.string,
};

PlanetInfo.propTypes = {
  isLoading: PropTypes.bool,
  name: PropTypes.string,
  population: PropTypes.string,
  url: PropTypes.string.isRequired,
};
