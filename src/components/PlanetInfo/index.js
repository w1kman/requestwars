import { connect } from 'react-redux';
import PlanetInfo from './PlanetInfo';
import {
  getIsLoading,
  getHomeWorldName,
  getHomeWorldPopulation,
} from 'store/homeWorld/reselect';

function mapStateToProps(state, props) {
  return {
    isLoading: getIsLoading(state, props),
    name: getHomeWorldName(state, props),
    population: getHomeWorldPopulation(state, props),
  }
}

export default connect(mapStateToProps)(PlanetInfo);
