import styled from 'styled-components';

export const PlanetText = styled('span')`
  font-weight: 600;
`;

export const PopulationText = styled('span')`
  color: #5e95c0;
`;
