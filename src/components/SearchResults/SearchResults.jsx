import React from 'react';
import PropTypes from 'prop-types';
import Container, { ResultWrapper } from './styled';
import Pagination from '../Pagination';
import PersonCard from '../PersonCard';

/**
 * Displays search results (stored in 'search' store).
 * @param {Object[]} items
 * @param {number} pages
 * @param {number} current
 * @param {function} onGotoPage
 */
export default function SearchResult({ items, pages, current, onGotoPage, setActiveItem, activeItem }) {
  return (
    <Container>

      <Pagination centered current={current} pages={pages} onGotoPage={onGotoPage}/>

      <ResultWrapper>
        {items.map(({ name, url, homeworld, species, films }) => {
          return (
            <PersonCard
              active={(url === activeItem)}
              key={url}
              name={name}
              homeworld={homeworld}
              species={species}
              films={films}
              url={url}
              onGotoPage={onGotoPage}
              setActiveItem={setActiveItem}
              activeItem={activeItem}
            />
          );
        })}
      </ResultWrapper>

      <Pagination centered current={current} pages={pages} onGotoPage={onGotoPage}/>

    </Container>
  );
}

SearchResult.defaultProps = {
  items: [],
  pages: null,
  current: null,
};

SearchResult.propTypes = {
  items: PropTypes.array,
  pages: PropTypes.number,
  current: PropTypes.number,
  activeItem: PropTypes.string,
  onGotoPage: PropTypes.func.isRequired,
  setActiveItem: PropTypes.func.isRequired,
};
