import SearchResults from './SearchResults';
import {connect} from 'react-redux';
import {getAllPersons, getPages, getCurrent, getActiveItem} from 'store/search/reselect';
import { gotoSearchPage, setActiveItem } from 'store/search/actions';

function mapStateToProps(state) {
  return {
    items: getAllPersons(state),
    pages: getPages(state),
    current: getCurrent(state),
    activeItem: getActiveItem(state),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onGotoPage(page) {
      dispatch(gotoSearchPage(page));
    },
    setActiveItem(itemURL) {
      dispatch(setActiveItem(itemURL));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults);
