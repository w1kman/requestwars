import FilmList from './FilmList';
import { connect } from 'react-redux';
import {getOrderedFilmData} from 'store/films/reselect';
import {fetchFilm} from 'store/films/actions';

function mapStateToProps(state, props) {
  return {
    orderedData: getOrderedFilmData(state, props),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchFilm(url) {
      dispatch(fetchFilm(url));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilmList);
