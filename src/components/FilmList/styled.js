import styled from 'styled-components';
export default styled('div')`
  margin-top: 1em;
  padding: 1em;
  border-radius: 4px;
  background-color: rgba(193, 213, 230, 0.15);
`;
