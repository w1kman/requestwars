import React from 'react';
import PropTypes from 'prop-types';
import LoadingIndicator from 'common/LoadingIndicator';
import FilmInfo from '../FilmInfo';
import FilmListContainer from './styled';

export default class FilmList extends React.Component {
  /**
   * Fetch films that aren't already fetched (so that we can order them)
   */
  componentDidMount() {
    const { films, orderedData, fetchFilm } = this.props;
    const loadedFilms = orderedData.map(film => film.url);

    films.forEach(film => {
      if (loadedFilms.indexOf(film) === -1) {
        fetchFilm(film);
      }
    });
  }

  render() {
    const { orderedData, films } = this.props;
    const loading = (orderedData.length !== films.length);
    if (loading) {
      return <LoadingIndicator/>
    }
    return (
        <FilmListContainer>
          {orderedData.map(
            ({ url, title, release_date, opening_crawl }) => (
              <FilmInfo
                key={url}
                title={title}
                releaseDate={release_date}
                crawl={opening_crawl}
              />
            )
          )}
        </FilmListContainer>
    );
  }
}

FilmList.defaultProps = {
  orderedData: [],
};

FilmList.propTypes = {
  orderedData: PropTypes.array,
  fetchFilm: PropTypes.func.isRequired,
};
