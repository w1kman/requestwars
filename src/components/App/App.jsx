import * as React from 'react';
import AppContainer from './styled';
import Search from '../Search';
import Section from 'common/Section';
import AppHeading from '../AppHeading';
import SearchResults from '../SearchResults';
import WarpSpeedCanvas from 'components/WarpSpeedCanvas';
import FixedContainer from 'common/FixedContainer';

export default function App() {
  return (
    <>
      <FixedContainer>
        <WarpSpeedCanvas/>
      </FixedContainer>

      <FixedContainer>
        <AppContainer>
          <Section style={{ justifyContent: 'center' }}>
            <AppHeading>XequestWZrs</AppHeading>
          </Section>

          <Section style={{ justifyContent: 'center' }}>
            <Search/>
          </Section>

          <Section>
            <SearchResults/>
          </Section>
        </AppContainer>
      </FixedContainer>
    </>
  );
}
