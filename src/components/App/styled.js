import styled from 'styled-components';

export default styled('div')`
  height: 100%;
  overflow: auto;
  padding-top: 1em;
`;
