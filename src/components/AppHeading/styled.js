import styled from 'styled-components';
import starwarsFont from 'common/css/starwarsFont';

export const Version = styled('span')`
  font-size: 1.5vw;
  display: flex;
  
  @media(min-width: 768px) {
    font-size: 1rem;
  }
`;

export default styled('h1')`
  ${starwarsFont};
  margin: 2rem 0;
  font-size: 8vw;
  text-shadow: 0 10px 20px rgba(0,0,0,0.6);
  position: relative;
  
  @media(min-width: 768px) {
    font-size: 4em;
  }
  
  & ${Version} {
    display: inline-block;
    position: absolute;
    bottom: 0;
    right: 0;
    color: #ff3f57;
    // background: rgba(0,0,0,0.47);
    padding: 0.25em 0.5em;
    border-radius: 0.5em;
    text-shadow: 1px 1px 0 #34121a;
    line-height: 100%;
  }
`;


