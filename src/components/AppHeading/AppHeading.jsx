import React from 'react';
import AppHeadingElement, {Version} from './styled';
import PropTypes from 'prop-types';

export default function AppHeading({ children }) {
  return (
    <AppHeadingElement>
      {children || 'Untitled'}
      <Version>Wookiee Powered</Version>
    </AppHeadingElement>
  );
}

AppHeading.propTypes = {
  children: PropTypes.any,
};
