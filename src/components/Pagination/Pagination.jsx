import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Button from 'common/Button';
import WrapperElement from './styled';

export default function Pagination({ pages, current, onGotoPage, centered }) {
  if (pages <= 1) return null;

  const onClickWrapper = (event) => {
    event.preventDefault();
    const { target } = event;
    if (target && (target.dataset.page !== undefined)) {
      onGotoPage(target.dataset.page);
    }
  };

  const pageItems = Array(pages || 0).fill(null);
  const showPrevNext = pages > 0;

  return (
    <WrapperElement onClick={onClickWrapper} centered={centered}>
      {showPrevNext &&  (
        <Button disabled={(current <= 1)} data-page={'prev'}>Prev</Button>
      )}
      {pageItems.map((_, index) => {
        const page = index + 1;
        const active = (page === current);
        return (
          <Button
            key={`${index}:${page}`}
            data-page={page}
            className={clsx({ active })}
          >
            {page}
          </Button>
        )
      })}
      {showPrevNext && (
        <Button disabled={current >= pages} data-page={'next'}>Next</Button>
      )}
    </WrapperElement>
  );
}

Pagination.defaultProps = {
  centered: false,
  pages: null,
  current: null,
};

Pagination.propTypes = {
  centered: PropTypes.bool,
  pages: PropTypes.number,
  current: PropTypes.number,
  onGotoPage: PropTypes.func.isRequired,
};
