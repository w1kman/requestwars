import styled from 'styled-components';

export default styled('div')`
  position: relative;
  text-align: ${props => props.centered ? 'center' : 'initial'};
  padding: 0.5em 0;
`;
