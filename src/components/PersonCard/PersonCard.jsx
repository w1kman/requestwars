import React from 'react';
import PropTypes from 'prop-types';
import CardElement, { CardTitle } from './styled';
import PlanetInfo from '../PlanetInfo';
import SpeciesInfo from '../SpeciesInfo';
import FilmList from '../FilmList';

export default class PersonCard extends React.Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);

    this.containerRef = React.createRef();
  }

  onClick() {
    const {url, setActiveItem} = this.props;
    setActiveItem(url);
    requestAnimationFrame(() => this.containerRef.current.scrollIntoView(true))
  }

  render() {
    const { name, homeworld, species, films, active} = this.props;
    return (
      <CardElement ref={this.containerRef} onClick={this.onClick} data-active={active}>
        <CardTitle>
          {name}
          {species.map(url => <SpeciesInfo key={url} url={url}/>)}
        </CardTitle>
        <PlanetInfo url={homeworld}/>
        {/*<div>Appears in {films.length} movie(s).</div>*/}
        {active &&  (
          <FilmList films={films}/>
        )}
      </CardElement>
    );
  }
}

PersonCard.defaultProps = {
  active: false,
  name: 'n/a',
  homeworld: null,
  species: [],
  films: [],
};

PersonCard.propTypes = {
  active: PropTypes.bool,
  name: PropTypes.string,
  homeworld: PropTypes.string,
  species: PropTypes.arrayOf(PropTypes.string),
  films: PropTypes.arrayOf(PropTypes.string),
  url: PropTypes.string.isRequired,
  setActiveItem: PropTypes.func.isRequired,
};
