import styled from 'styled-components';
import Card from 'common/Card';

export default styled(Card)`
  position: relative;
  margin: 0.25rem;
  overflow: hidden;
  transform: scale(1);
  transition: all 0.3s ease-in-out;
  cursor: pointer;
  
  &[data-active=false] {
    transform: scale(0.95);  
  }
  
  &[data-active=true] {
    border-top: 4px #ff3f57 solid;
  }
  
  &[data-active=false]:active {
    transform: scale(0.98) rotate(0.6deg) !important;  
  }
  
  &[data-active=false]:hover {
    transform: scale(1) rotate(1deg);
    box-shadow: 3px 5px 20px -9px rgba(0,0,0,0.45);
    z-index: 3;
  }
`;

export const CardTitle = styled('h2')`
  margin: 0;
  color: #333333;
`;

