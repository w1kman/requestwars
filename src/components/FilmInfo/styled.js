import styled from 'styled-components';

export default styled('div')`
  margin: 0.5rem 0;
  padding-top: 0.5rem;
  border-top: 1px #000 dashed;
  
  & h3 {
    margin: 0 0 0.25rem 0;
  }
  
  &:first-of-type {
    padding-top: 0;
    border: none;  
  }
`;

export const OpeningCrawl = styled('div')`
  margin-top: 0.5em;
  border-left: 4px #6d6d6d solid;
  padding-left: 0.5em;
  font-size: 1.1rem;
`;
