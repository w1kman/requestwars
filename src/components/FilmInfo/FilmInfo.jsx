import React from 'react';
import PropTypes from 'prop-types';
import truncateString from 'helpers/truncateStringDeluxe';
import FilmInfoContainer, {OpeningCrawl} from './styled';

export default function FilmInfo({title, releaseDate, crawl}) {
  return (
    <FilmInfoContainer>
      <h3>{title}</h3>
      <div style={{fontSize: '0.7rem', color: '#5e95c0'}}>Release date: {releaseDate}</div>
      <OpeningCrawl>{truncateString(crawl, 150)}</OpeningCrawl>
    </FilmInfoContainer>
  );
}

FilmInfo.propTypes = {
  title: PropTypes.string.isRequired,
  releaseDate: PropTypes.string.isRequired,
  crawl: PropTypes.string.isRequired,
};
