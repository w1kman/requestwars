import styled from 'styled-components';

export default styled('span')`
  display: inline-block;
  font-size: 0.6rem;
  background: #454545;
  border-radius: 0.5rem;
  padding: 0.25rem 0.5rem;
  margin: 0 0.5rem;
  position: relative;
  top: -4px;
  color: #fff;
`;
