import React from 'react';
import PropTypes from 'prop-types';
import InfoElement from './styled';

export default function SpeciesInfo({ isLoading, name }) {
  return <InfoElement>{name}</InfoElement>;
}

SpeciesInfo.defaultProps = {
  isLoading: true,
  name: 'n/a'
};
SpeciesInfo.propTypes = {
  url: PropTypes.string.isRequired,
  isLoading: PropTypes.bool,
  name: PropTypes.string,
};
