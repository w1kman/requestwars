import { connect } from 'react-redux';
import SpeciesInfo from './SpeciesInfo';

import {
  getIsLoading,
  getSpeciesName,
} from 'store/species/reselect';

function mapStateToProps(state, props) {
  return {
    isLoading: getIsLoading(state, props),
    name: getSpeciesName(state, props),
  }
}

export default connect(mapStateToProps)(SpeciesInfo);
