import {createGlobalStyle} from 'styled-components';

export default createGlobalStyle`
  #root,
  body {
    position: relative;
    height: 100%;
    width: 100%;
  }
  body {
    margin: 0;
    font-family: /* star_jediregular */ -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background: #f6f6f6;
  }
  
  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New",
      monospace;
  }  
`;
