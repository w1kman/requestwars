import { handleActions } from 'redux-actions';
import defaultState from './state';
import {
  searchCharacters,
  searchCharactersSuccess,
  searchCharactersError,
  gotoSearchPage,
  gotoSearchPageError,
  gotoSearchPageSuccess,
  setActiveItem,
} from './actions';

/**
 * Order items by name
 * @param {object[]} people
 * @returns {object[]}
 */
function orderPeopleByName(people) {
  return [...people].sort((a, b) => {
    // This may be a bit confusing since swapi.co does not support ordering
    // Basically a multi page result will not be ordered from one page to
    // another. Only the page it self will be ordered.
    const nameA = a.name.toLowerCase();
    const nameB = b.name.toLowerCase();
    if (nameA === nameB) return 0;
    return (nameA > nameB) ? 1: -1;
  });
}

/**
 * Update the state with new items
 * @param state
 * @param payload
 * @returns {{isSearching: boolean, items: Object[]}}
 */
function updateSearchResult(state, { payload }) {
  return {
    ...state,
    ...payload,
    activeItem: '',
    items: orderPeopleByName(payload.items),
    isSearching: false,
  };
}

export default handleActions({
  [searchCharacters]: (state, { payload }) => ({
    ...state,
    query: String(payload),
    isSearching: true,
  }),
  [searchCharactersSuccess]: updateSearchResult,
  [gotoSearchPageSuccess]: updateSearchResult,
  [searchCharactersError]: (state) => ({
    ...defaultState,
    query: state.query,
  }),
  [gotoSearchPage]: (state) => ({
    ...state,
    isSearching: true,
  }),
  [gotoSearchPageError]: (state) => ({
    ...defaultState,
    query: state.query,
  }),
  [setActiveItem]: (state, {payload}) => ({
    ...state,
    activeItem: payload,
  }),
}, defaultState);
