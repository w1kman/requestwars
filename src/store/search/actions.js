import { createActions } from 'redux-actions';

export const {
  searchCharacters,
  searchCharactersSuccess,
  searchCharactersError,
  gotoSearchPage,
  gotoSearchPageError,
  gotoSearchPageSuccess,
  setActiveItem,
} = createActions(
  'SEARCH_CHARACTERS',
  'SEARCH_CHARACTERS_SUCCESS',
  'SEARCH_CHARACTERS_ERROR',
  'GOTO_SEARCH_PAGE',
  'GOTO_SEARCH_PAGE_SUCCESS',
  'GOTO_SEARCH_PAGE_ERROR',
  'SET_ACTIVE_ITEM'
);
