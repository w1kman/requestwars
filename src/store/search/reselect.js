import { createSelector } from 'reselect';
export const getIsSearching = (state) => (state.search.isSearching);
export const getAllPersons = (state) => (state.search.items);
export const getPages = (state) => (state.search.pages);
export const getCurrent = (state) => (state.search.current);
export const getActiveItem = (state) => (state.search.activeItem);

export const getAllPlanetsFromPersons = createSelector(
  [getAllPersons],
  (persons) => {
    const planets = new Set(persons.map(person => person.homeworld));
    return [...planets];
  }
);

export const getAllSpeciesFromPersons = createSelector(
  [getAllPersons],
  (persons) => {
    const species = [];
    persons.forEach(person => species.push(...person.species));
    return [...(new Set(species))];
  }
);
