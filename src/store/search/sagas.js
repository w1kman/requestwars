import { select, put, call, takeLatest } from 'redux-saga/effects';
import {
  searchCharacters,
  searchCharactersSuccess,
  searchCharactersError,
  gotoSearchPage,
  gotoSearchPageError,
  gotoSearchPageSuccess,
} from './actions';
import { fetchMissingPlanets } from '../homeWorld/actions';
import { fetchMissingSpecies } from '../species/actions';
import SWAPI from 'swapi';

/**
 * Fetch swapi/peaople from SWAPI
 * @param {*} action
 * @returns {IterableIterator<AllEffect<SimpleEffect<"PUT", PutEffectDescriptor<*>>>|CallEffect|SelectEffect|PutEffect<*>>}
 */
function* doSearchCharacters(action) {
  try {
    const result = yield call(SWAPI.people().search(action.payload).get);
    yield put(searchCharactersSuccess(result));
    yield put(fetchMissingPlanets());
    yield put(fetchMissingSpecies());
  }
  catch (error) {
    yield put(searchCharactersError());
  }
}

function* doGotoSearchPage({ payload }) {
  const state = yield select();
  const { requestURL } = state.search;
  try {
    if (!requestURL) {
      // noinspection ExceptionCaughtLocallyJS
      throw new Error('Unable to goto page. Request URL cannot be empty.');
    }
    const result = yield call(SWAPI.get, requestURL, payload);
    yield put(gotoSearchPageSuccess(result));
    yield put(fetchMissingPlanets());
    yield put(fetchMissingSpecies());
  }
  catch (error) {
    yield put(gotoSearchPageError(error));
  }
}

export default function* searchSagas() {
  yield takeLatest(searchCharacters, doSearchCharacters);
  yield takeLatest(gotoSearchPage, doGotoSearchPage);
}
