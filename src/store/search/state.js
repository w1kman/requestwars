/**
 * Search - Default state
 */
export default {
  query: '',
  isSearching: false,
  requestURL: '',
  count: null,
  pages: null,
  current: null,
  previous: null,
  next: null,
  items: [],
  activeItem: '',
}
