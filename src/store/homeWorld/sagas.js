import { all, put, call, takeEvery, select } from 'redux-saga/effects';
import SWAPI from 'swapi';
import {
  fetchHomeWorld,
  fetchHomeWorldError,
  fetchHomeWorldSuccess,
  fetchMissingPlanets,
} from './actions';
import {getMissingPlanets} from './reselect';

function* doFetchHomeWorld({payload}) {
  try {
    const result = yield call(SWAPI.get, payload);
    yield put(fetchHomeWorldSuccess(result));
  } catch (error) {
    yield put(fetchHomeWorldError());
  }
}

function* doFetchMissingPlanets() {
  const state = yield select();
  const missingPlanets = getMissingPlanets(state);
  yield all(missingPlanets.map(planet => put(fetchHomeWorld(planet))));
}

export default function* homeWorldSagas() {
  yield takeEvery(fetchHomeWorld, doFetchHomeWorld);
  yield takeEvery(fetchMissingPlanets, doFetchMissingPlanets);
}

