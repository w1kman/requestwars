import { handleActions } from 'redux-actions';
import defaultState from './state';
import {
  fetchHomeWorldSuccess,
} from './actions';

export default handleActions({
  [fetchHomeWorldSuccess]: (state, {payload}) => {
    return {
      ...state,
      [payload.url]: payload,
    };
  },
}, defaultState);
