import { createActions } from 'redux-actions';

export const {
  fetchHomeWorld,
  fetchHomeWorldSuccess,
  fetchHomeWorldError,
  fetchMissingPlanets,
} = createActions(
  'FETCH_HOME_WORLD',
  'FETCH_HOME_WORLD_SUCCESS',
  'FETCH_HOME_WORLD_ERROR',
  'FETCH_MISSING_PLANETS',
);
