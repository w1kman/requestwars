import { createSelector } from 'reselect';
import { getAllPlanetsFromPersons } from '../search/reselect';
import formatNumber from 'helpers/formatNumber';

export const getHomeWorldNames = (state) => Object.keys(state.homeWorld);
export const getHomeWorld = (state, props) => (state.homeWorld[props.url] || false);
export const getIsLoading = createSelector(
  [getHomeWorld],
  homeWorld => !homeWorld
);

export const getHomeWorldName = createSelector(
  [getIsLoading, getHomeWorld],
  (isLoading, homeWorld) => {
    return (isLoading) ? '???' : homeWorld.name;
  }
);

export const getHomeWorldPopulation = createSelector(
  [getIsLoading, getHomeWorld],
  (isLoading, homeWorld) => {
    return (isLoading) ? '???' : formatNumber(homeWorld.population);
  }
);

export const getMissingPlanets = createSelector(
  [getHomeWorldNames, getAllPlanetsFromPersons,],
  (loadedPlanets, requiredPlanets) => (requiredPlanets.filter(planet => loadedPlanets.indexOf(planet) === -1)),
);
