import {combineReducers} from 'redux';
import search from './search/reducer';
import homeWorld from './homeWorld/reducer';
import species from './species/reducer';
import films from './films/reducer';

export default combineReducers({
  search,
  homeWorld,
  species,
  films,
});
