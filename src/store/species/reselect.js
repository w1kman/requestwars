import { createSelector } from 'reselect';
import { getAllSpeciesFromPersons } from '../search/reselect';

export const getSpeciesNames = (state) => (Object.keys(state.species || {}));

export const getSpecies = (state, props) => (state.species[props.url] || false);

export const getIsLoading = createSelector(
  [getSpecies],
  species => !species
);

export const getSpeciesName = createSelector(
  [getIsLoading, getSpecies],
  (isLoading, species) => {
    return (isLoading && species) ? `DL: ${species}` : species.name;
  }
);

export const getMissingSpecies = createSelector(
  [getSpeciesNames, getAllSpeciesFromPersons,],
  (loadedSpecies, requiredSpecies) => (requiredSpecies.filter(species => loadedSpecies.indexOf(species) === -1)),
);
