import { handleActions } from 'redux-actions';
import defaultState from './state';
import {
  fetchSpeciesError,
  fetchSpeciesSuccess,
} from './actions';

export default handleActions({
  [fetchSpeciesError]: (state, {payload}) => {
    console.error(fetchSpeciesError.toString(), payload);
    return state;
  },
  [fetchSpeciesSuccess]: (state, {payload}) => {
    return {
      ...state,
      [payload.url]: payload,
    };
  },
}, defaultState);
