import { all, put, call, takeEvery, select } from 'redux-saga/effects';
import {getMissingSpecies} from './reselect';
import SWAPI from 'swapi';
import {
  fetchSpecies,
  fetchSpeciesError,
  fetchSpeciesSuccess,
  fetchMissingSpecies,
} from './actions';

function* doFetchSpecies({payload}) {
  try {
    const result = yield call(SWAPI.get, payload);
    yield put(fetchSpeciesSuccess(result));
  } catch (error) {
    yield put(fetchSpeciesError());
  }
}

function* doFetchMissingSpecies() {
  const state = yield select();
  const missingSpecies = getMissingSpecies(state);
  yield all(missingSpecies.map(species => put(fetchSpecies(species))));
}

export default function* speciesSagas() {
  yield takeEvery(fetchSpecies, doFetchSpecies);
  yield takeEvery(fetchMissingSpecies, doFetchMissingSpecies);
}
