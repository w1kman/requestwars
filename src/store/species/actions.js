import { createActions } from 'redux-actions';

export const {
  fetchSpecies,
  fetchSpeciesSuccess,
  fetchSpeciesError,
  fetchMissingSpecies,
} = createActions(
  'FETCH_SPECIES',
  'FETCH_SPECIES_SUCCESS',
  'FETCH_SPECIES_ERROR',
  'FETCH_MISSING_SPECIES'
);
