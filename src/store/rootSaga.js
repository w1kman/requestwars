import { fork } from 'redux-saga/effects'
import searchSagas from './search/sagas';
import homeWorldSagas from './homeWorld/sagas';
import speciesSagas from './species/sagas';
import filmsSagas from './films/sagas';

export default function* root() {
  yield fork(searchSagas);
  yield fork(homeWorldSagas);
  yield fork(speciesSagas);
  yield fork(filmsSagas);
}
