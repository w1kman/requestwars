import { createActions } from 'redux-actions';

export const {
  fetchFilm,
  fetchFilmError,
  fetchFilmSuccess,
} = createActions(
  'FETCH_FILM',
  'FETCH_FILM_SUCCESS',
  'FETCH_FILM_ERROR',
);
