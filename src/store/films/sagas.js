import { put, call, takeEvery } from 'redux-saga/effects';
import SWAPI from 'swapi';
import {
  fetchFilm,
  fetchFilmSuccess,
  fetchFilmError,
} from './actions';

function* doFetchFilm({payload}) {
  try {
    const result = yield call(SWAPI.get, payload);
    yield put(fetchFilmSuccess(result));
  } catch (error) {
    yield put(fetchFilmError());
  }
}

export default function* homeWorldSagas() {
  yield takeEvery(fetchFilm, doFetchFilm);
}

