import { handleActions } from 'redux-actions';
import defaultState from './state';
import {
  fetchFilmSuccess,
} from './actions';

export default handleActions({
  [fetchFilmSuccess]: (state, {payload}) => {
    return {
      ...state,
      [payload.url]: payload,
    };
  },
}, defaultState);
