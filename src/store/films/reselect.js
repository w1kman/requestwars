export const getFilm = (state, props) => (state.films[props.url]);

export const getOrderedFilmData = (state, props) => {
  const { films = [] } = props;
  let data = films
    .filter(url => state.films[url] !== undefined)
    .map(url => state.films[url]);
  data.sort((a, b) => {
    if (a.release_date === b.release_date) return 0;
    return (a.release_date > b.release_date) ? 1 : -1;
  });
  return data;
};
