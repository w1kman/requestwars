import styled from 'styled-components';

export default styled('div')`
  background: #fff;
  padding: 1em;
  border-radius: 3px;
`;
