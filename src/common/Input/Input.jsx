import React from 'react';
import PropTypes from 'prop-types';
import InputElement from './styled';

export default function Input({type, ...props}) {
  return <InputElement type={type} {...props}  />;
}

Input.defaultProps = {
  type: 'text',
};

Input.propTypes = {
  type: PropTypes.oneOf([
    'button',
    'checkbox',
    'color',
    'date',
    'datetime-local',
    'email',
    'file',
    'hidden',
    'image',
    'month',
    'number',
    'password',
    'radio',
    'range',
    'reset',
    'search',
    'submit',
    'tel',
    'text',
    'time',
    'url',
    'week',
  ]).isRequired,
};
