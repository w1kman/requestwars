import styled from 'styled-components';

export default styled('input')`
  border: 0;
  padding: 0.5em;
  border-radius: 3px;
  outline: 0;
  font-size: 18px;
  box-shadow: 1px 1px 0 #7e7e7e;
  background: #f9f9f9;  
  transition: background 0.3s;
  
  &:hover,
  &:focus {
    background: #fff;  
  }
`;
