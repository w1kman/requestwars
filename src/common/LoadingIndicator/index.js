import React from 'react';
import LoadingIndicatorElement from './styled';
export default function LoadingIndicator() {
  return (
    <LoadingIndicatorElement>
      <div/><div/><div/>
    </LoadingIndicatorElement>
  );
}
