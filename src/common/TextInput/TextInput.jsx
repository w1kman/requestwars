import React from 'react';
import PropTypes from 'prop-types';

import TextInputElement from './styled';

export default function TextInput({value, onChange, forwardedRef, ...props}) {
  return <TextInputElement type="text" ref={forwardedRef} value={value} onChange={onChange} {...props}/>
}

TextInput.defaultProps = {
  value: '',
  onChange: () => {},
};

TextInput.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onChange: PropTypes.func,
};

