import styled from 'styled-components';

export default styled('button')`
  border: 0;
  background: #ff3f57;
  outline: 0;
  color: #ffffff;
  font-weight: 100;
  padding: 0.5em 1em;
  cursor: pointer;
  border-radius: 0.25em;
  margin: 0 0.15em;
  box-shadow: 1px 1px 0 #8d3e47;
  text-shadow: 1px 1px #8d3e47;
  transition: background 0.3s;

  &:active {
    transform: translate(0, 2px);
    //box-shadow: none !important;
    box-shadow: 1px 1px 0 #f45498 inset !important;
  }

  &:hover {
    background: #bc3d54;
    box-shadow: 1px 1px 1px #8d3e47;  
  }
  &[disabled] {
    background: #d8d8d8;
    box-shadow: 1px 1px 1px #a9a9a9; 
    color: #545454;
    text-shadow: none;
    cursor: initial;
  }
  
  &.active {
    background: #bc3d54;
  }
`;
