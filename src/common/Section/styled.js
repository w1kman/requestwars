import styled from 'styled-components';

export default styled('section')`
    margin: 0 auto;
    max-width: 980px;
    display: flex;
`;
