import endpoints from './endpoints';
import { PEOPLE_ENDPOINT } from './constants';

const SYM_ACTIVE_ENDPOINT = Symbol('SYM_ACTIVE_ENDPOINT');

/**
 * Limited helper class for fetching data from https://swapi.co
 */
export class SWAPI {
  /**
   * Returns page number from URL
   * @param {string} url
   * @returns {{url: string, page: Number}}
   */
  static getPageNum(url) {
    const [baseURL, searchString = ''] = url.split('?');
    const params = new URLSearchParams(searchString);
    if (!params.has('page')) {
      params.set('page', '1');
    }
    return {
      url: `${baseURL}?${params}`,
      page: Number(params.get('page')),
    };
  }

  /**
   * Set the page request param
   * @param url
   * @param page
   * @returns {{page: number, url: string}}
   */
  static setPage(url, page) {
    const [baseURL, searchString = ''] = url.split('?');
    const params = new URLSearchParams(searchString);
    let pageNum = null;
    let currentPage = null;
    if (page === 'next') {
      currentPage = SWAPI.getPageNum(url).page;
      pageNum = currentPage + 1;
    } else if (page === 'prev') {
      currentPage = SWAPI.getPageNum(url).page;
      pageNum = currentPage - 1;
      if (pageNum < 1) pageNum = 1;
    } else {
      pageNum = page;
    }
    params.set('page', String(pageNum));
    return {
      url: `${baseURL}?${params}`,
      page: Number(params.get('page')),
    };
  }

  constructor() {
    this.baseURL = 'https://swapi.dev/api';
    this[SYM_ACTIVE_ENDPOINT] = null;
    this.get = this.get.bind(this);
  }

  get endpoint() {
    return this[SYM_ACTIVE_ENDPOINT];
  }

  /**
   * Set active endpoint
   * @param {string} endpointId
   */
  setEndpoint(endpointId) {
    const endpoint = endpoints[endpointId];
    if (!endpoint) {
      throw new Error(`Endpoint '${endpointId}' does not exist.`);
    }
    this[SYM_ACTIVE_ENDPOINT] = new endpoint(this);
    return this.endpoint;
  }

  /**
   *
   * @returns {People}
   */
  people() {
    return this.setEndpoint(PEOPLE_ENDPOINT);
  }

  getNormalizedURL(requestURL) {
    const url = new URL(requestURL || this.getRequestURL());
    const siteURL = new URL(window.location.href);

    // Match protocol
    url.protocol = siteURL.protocol;

    return url.href;
  }

  get(requestURL = null, page = null) {
    let url = this.getNormalizedURL(requestURL);
    if (page !== null) url = SWAPI.setPage(url, page).url;
    return fetch(
      url,
      {
        method: 'GET',
        mode: 'cors',
        headers: {
        'Content-Type': 'application/json'
        }
      })
    .then(function (response) {
      const contentType = response.headers.get('content-type');
      if (contentType && contentType.includes('application/json')) {
        return response.json();
      }
      throw new TypeError('OMG! You broke it! (response is not JSON)');
    })
    .then(function (json) {
      const { results: items, ...rest } = json;
      const pageNum = SWAPI.getPageNum(url);
      return {
        ...rest,
        items,
        requestURL: pageNum.url,
        current: pageNum.page,
        pages: Math.ceil(json.count / 10),
      }
    })
    .catch(function (error) {
      console.error('SWAPI Error', error);
    });
  }

  /**
   * Returns request url
   * @returns {string}
   */
  getRequestURL() {
    return `${this.baseURL}${this.endpoint.path}${this.endpoint.request}`;
  }
}

export default new SWAPI();
