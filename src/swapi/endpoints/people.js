import { SWAPI } from '../';

const SYM_SWAPI = Symbol('SYM_SWAPI');
export default class People {

  constructor(swapi) {
    if (!(swapi instanceof SWAPI)) {
      throw new Error(
        'Could not create instance of People. Constructor param is not instance of SWAPI.'
      );
    }
    this[SYM_SWAPI] = swapi;
    this.path = '/people/';
    this.request = '';
  }

  get swapi() {
    return this[SYM_SWAPI];
  }

  /**
   * Search Fields: name
   * @param {string} query
   */
  search(query) {
    const encodedQuery = encodeURIComponent(String(query));
    if (encodedQuery === '') return this.list();
    this.request = `?search=${encodedQuery}`;
    return this.swapi;
  }

  /**
   * List all items
   */
  list() {
    this.request = '';
    return this.swapi;
  }

  /**
   * Get a specific person by id
   * @TODO Requires react-router-dom to be useful
   */
  byId(id) {
    if (Number(id) !== id) {
      throw new Error('Person.byId requires id param to be of type Number.');
    }
    this.request = `${id}/`;
    return this.swapi;
  }

}
