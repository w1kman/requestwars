import {PEOPLE_ENDPOINT} from '../constants';
import people from './people';

export default {
  [PEOPLE_ENDPOINT]: people,
}
